
var has =  function (object, key) {
      return object ? hasOwnProperty.call(object, key) : false;
   }

$.fn.initExcelider = function(customOpts)
{

	var defaultOpts = {

		"defaultWidth":1200,
		"defaultHeight":700,
		"animTimeoutTime":500,
		"delayTimeoutTime":5000,
		"numberofIndiv":0,
		"fullWidthPercent":100,
		"pauseOnHover":false,
		"responsive":true,
		"numberOfDroplets":0,
		"portraitMode":true,
		"portraitTheme":"dark",
		"touchEnabled":true,
		"autoPlay":true,
		"currentClickId":"",
		"animateType":'slide',
		"showPlayControls":true,
		"playControls":{
			"position":"topright"
		},
		"showNav":true,
		"navOptions":{
			"autoHide":false
		},
		"showSlidePoint":true,
		"slidePoint":{
		"pointType":"square",
		"borderColor":"rgba(0,0,0,0)",
		"color":"rgb(255,255,255)",
		"currentBorderColor":"rgb(255,255,255)",
		"currentColor":"rgb(0,0,0)",
		"noBorder":true
	}
	}

	//For Level0
	$.each(defaultOpts,function(key,value)
	{
		if(!has(customOpts,key))
		{
			customOpts[key] = value;
		}
	})
	
	//For Level1
	$.each(defaultOpts.playControls,function(key,value)
	{
		if(!has(customOpts.playControls,key))
		{
			customOpts.playControls[key] = value;
		}
	})
	$.each(defaultOpts.navOptions,function(key,value)
	{
		if(!has(customOpts.navOptions,key))
		{
			customOpts.navOptions[key] = value;
		}
	})
	$.each(defaultOpts.slidePoint,function(key,value)
	{
		if(!has(customOpts.slidePoint,key))
		{
			customOpts.slidePoint[key] = value;
		}
	})
	console.log(customOpts);
	var currentClickId = "#" + $(this).attr('id');
	var cid = "#" + $(this).attr('id');
	var justId = $(this).attr('id');
	var notedBody = '';
	var pausePlay = false;
	//$().initAllEx();
	var obj = {};
	var globalObj = {}; 
	var globalObject = new parentClass(this, customOpts,cid,justId,notedBody,pausePlay);  
	
	globalObject.elem = this;
	globalObject.options = customOpts;
	globalObject.cid = cid;
	globalObject.notedBody = "";
	globalObject.methods.initAllEx(true);
	console.log(globalObject);
/*	$(window).resize(

	function()
	{
		setTimeout(
				function()
				{
					globalObject.methods.initAgain();
				},2000);
		
	})*/
	//return instances[justId].methods.initAllEx();

}


var rgb2hex = function (rgb){
 rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
 return (rgb && rgb.length === 4) ? "" +
  ("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
  ("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
  ("0" + parseInt(rgb[3],10).toString(16)).slice(-2) : '';
}
function touchHandler(event)
{
    var touches = event.changedTouches,
        first = touches[0],
        type = "";
    switch(event.type)
    {
        case "touchstart": type = "mousedown"; break;
        case "touchmove":  type = "mousemove"; break;        
        case "touchend":   type = "mouseup";   break;
        default:           return;
    }

    // initMouseEvent(type, canBubble, cancelable, view, clickCount, 
    //                screenX, screenY, clientX, clientY, ctrlKey, 
    //                altKey, shiftKey, metaKey, button, relatedTarget);

    var simulatedEvent = document.createEvent("MouseEvent");
    simulatedEvent.initMouseEvent(type, true, true, window, 1, 
                                  first.screenX, first.screenY, 
                                  first.clientX, first.clientY, false, 
                                  false, false, false, 0/*left*/, null);

    first.target.dispatchEvent(simulatedEvent);
    //event.preventDefault();
}
function init() 
{
    document.addEventListener("touchstart", touchHandler, true);
    document.addEventListener("touchmove", touchHandler, true);
    document.addEventListener("touchend", touchHandler, true);
    document.addEventListener("touchcancel", touchHandler, true);    
}
//init();

var mouseEnterLeaveHandler = function()
{
	var currentID = $(this).attr('id');
	var enterLeaveId ;
	var mouseEnterHandleAlone = function(e)
	{
		currentID = $(this).attr('id');
		enterLeaveId = currentID;
		if($("#"+currentID).attr('pauseOnHover')=="true")
		{
			$("#"+currentID).attr('pauseplay',true);
			$('#'+currentID+" .playToggle i").attr('class','fa fa-play');
		}
	}
	var mouseLeaveHandleAlone = function(e)
	{
		if($("#"+currentID).attr('pauseOnHover')=="true")
		{
			$("#"+currentID).attr('pauseplay',false);
			$('#'+currentID+" .playToggle i").attr('class','fa fa-pause');
		}
	}
	$('.ex_viewport').on('mouseenter',mouseEnterHandleAlone);
	$('.ex_viewport').on('mouseleave',mouseLeaveHandleAlone);
}

var mouseUp = false;

var clickAndDrag = function(justId)
{
	var clickFlag = false;
	var moveFlag = false;
	var clickOffset = {};
	var moveOffset = {};
	var currentID = "";
	var revertBackOffset = {};

	var mouseEnterHandle = function(e)
	{
		currentID = $(this).attr('id');
	}
	var mouseDownHandle = function(e)
		{
			currentID = $(this).attr('id');
			clickFlag = true;
			clickOffset = e.clientX;
			revertBackOffset = $('#'+currentID+' .ex_li_indiv.activeliMain').position();
		}
	var mouseMoveHandle = function(e)
		{
			if(clickFlag)
			{
				moveFlag = true;
				moveOffset = e.clientX;
				//console.log('Dragging');
				//console.log(clickOffset-moveOffset);
				/*if(clickOffset>moveOffset)
				{
					var currentOffset = $('#'+currentID+' .ex_li_indiv.activeliMain').position();
					console.log("Dragging Left");
					$('#'+currentID+' .ex_li_indiv.activeliMain').position({ top:currentOffset.top, left: currentOffset.left+(clickOffset-moveOffset) })
				}
				else
				{
					console.log("Dragging Right");
					var currentOffset = $('#'+currentID+' .ex_li_indiv.activeliMain').position();
					$('#'+currentID+' .ex_li_indiv.activeliMain').position({ top:currentOffset.top, left: currentOffset.left+(clickOffset-moveOffset) })
				}*/
				
			}
			
			
		}
	var mouseLeaveHandle = function(e)
		{
			clickFlag = false;
			moveFlag = false;
		};
	
	var mouseUpHandle = function()
		{
			if(clickFlag&&moveFlag)
			{
				console.log(clickOffset);
				console.log(moveOffset);
				clickFlag = false;
				moveFlag = false;
				console.log(currentID);
				console.log("mouseMOved");
				console.log(clickOffset-moveOffset);
				if(Math.abs(clickOffset-moveOffset)>50)
				{
					if(clickOffset>moveOffset)
					{
						console.log("Moved LEFT");
						changeOneSlide("next",currentID)
					}
					else
					{
						console.log("Moved RIGHT");
						changeOneSlide("prev",currentID)
					}
				}
				
				
			}
			clickNumber = 0;
			//console.log($(this).attr('id'));
		}

	$("#"+justId+'.ex_viewport').on('mouseenter',mouseEnterHandle);
	$("#"+justId+'.ex_viewport').on('mousedown',mouseDownHandle);
	$("#"+justId+'.ex_viewport').on('mousemove',mouseMoveHandle)
	$("#"+justId+'.ex_viewport').on('mouseleave',mouseLeaveHandle)
	$("#"+justId+'.ex_viewport').on('mouseup',mouseUpHandle);

}
//clickAndDrag();
var loopedSub = function(scrollCount,cid,options_numberofIndiv,options_delayTimeoutTime,justId)
{
	//alert(cid)
				var colorString = $(cid+'-0').attr('data-colors');
				
				var reqColorsArr = colorString.split(',');
				var checkInterval = 0;
				console.log(reqColorsArr)
				var SliderAutoTimer;
				clearTimeout(SliderAutoTimer);

				var manipulatedTimeout = 0;
				var tempvar = $(cid +" .activeliMain").find('.exanim');
				var numberOfAnim = tempvar.length;
				for(var i=0;i<numberOfAnim;i++)
				{
					manipulatedTimeout = manipulatedTimeout + parseFloat($(tempvar[i]).attr('data-action'));
				}
				manipulatedTimeout = manipulatedTimeout + options_delayTimeoutTime;
				console.log(manipulatedTimeout);
				console.log($(cid +" .activeliMain"));
				SliderAutoTimer = setTimeout(
				function()
				{
					var pausePlay = $(cid).attr('pausePlay');
					var pausePlay = (pausePlay == 'true');
					console.log(pausePlay);
					var TempId = $(cid+' .activepoint').attr('id');
					var currentID = TempId.split('-');
					currentID = currentID[currentID.length-1]
					//scrollCount = currentID;
					//alert(currentID);


					if(!pausePlay)
					{
						currentID = parseInt(currentID) + 1;
						scrollCount = currentID + 1;
						console.log(scrollCount);
						console.log(scrollCount,options_numberofIndiv);
						if(scrollCount > options_numberofIndiv)
						{
							scrollCount = 0;
							//clearTimeout(SliderAutoTimer);
							pointerClick(justId+'-'+scrollCount,reqColorsArr[1],reqColorsArr[2],reqColorsArr[3],reqColorsArr[4],reqColorsArr[5],reqColorsArr[6],false,scrollCount,cid,options_numberofIndiv,options_delayTimeoutTime,justId);
							//loopedSub(scrollCount,cid,options,justId);
						}
						else
						{

							pointerClick(justId+'-'+(scrollCount-1),reqColorsArr[1],reqColorsArr[2],reqColorsArr[3],reqColorsArr[4],reqColorsArr[5],reqColorsArr[6],false,scrollCount,cid,options_numberofIndiv,options_delayTimeoutTime,justId);
							//loopedSub((scrollCount),cid,options,justId);
						}
					}
					else
					{
						currentID = parseInt(currentID);
						scrollCount = currentID + 1;
						checkInterval = setInterval(function(){
							var pausePlay = $(cid).attr('pausePlay');
							var pausePlay = (pausePlay == 'true');
							console.log("checking");
							if(!pausePlay)
							{
								clearInterval(checkInterval);
								currentID = parseInt(currentID) + 1;
								scrollCount = currentID + 1;
								if(scrollCount > options_numberofIndiv)
								{
									scrollCount = 0;
									//clearTimeout(SliderAutoTimer);
									pointerClick(justId+'-'+scrollCount,reqColorsArr[1],reqColorsArr[2],reqColorsArr[3],reqColorsArr[4],reqColorsArr[5],reqColorsArr[6],false,scrollCount,cid,options_numberofIndiv,options_delayTimeoutTime,justId);
									//loopedSub(scrollCount,cid,options,justId);
								}
								else
								{

									pointerClick(justId+'-'+(scrollCount-1),reqColorsArr[1],reqColorsArr[2],reqColorsArr[3],reqColorsArr[4],reqColorsArr[5],reqColorsArr[6],false,scrollCount,cid,options_numberofIndiv,options_delayTimeoutTime,justId);
									//loopedSub((scrollCount),cid,options,justId);
								}
							}
						},200);
					}
					



				},manipulatedTimeout)
}

console.log(loopedSub)

var playToggle = function(id)
{
	var playOrPause = $("#"+id).attr('pauseplay');
	playOrPause = (playOrPause == 'true');

	if(playOrPause)
	{
		$("#"+id+" .playToggle i").attr('class','');
		$("#"+id+" .playToggle i").attr('class','fa fa-pause');
		playOrPause = false;
		$("#"+id).attr('pausePlay',playOrPause);
	}
	else
	{
		$("#"+id+" .playToggle i").attr('class','');
		$("#"+id+" .playToggle i").attr('class','fa fa-play');
		playOrPause = true;
		$("#"+id).attr('pausePlay',playOrPause);
	}

}

var changeExtreme = function(position,id)
{
	var totalNumberOfSlides = 0;
	var colorString = $('#'+id+'-0').attr('data-colors');
	var reqColorsArr = colorString.split(',');

	var cid = '#'+id;

	var TempId = $('#'+id+' .activepoint').attr('id');
	var currentID = TempId.split('-');
	currentID = currentID[currentID.length-1]

	var scrollCount = parseInt(currentID) + 1;
	console.log(colorString);

		$('#'+id+' .list_pointers_ul li').each(
			function()
			{
				totalNumberOfSlides = totalNumberOfSlides + 1;
			});
	console.log(totalNumberOfSlides);
	var numberofIndiv = totalNumberOfSlides;
	var delayTimeoutTime = parseInt($(cid).attr('data-delaytimeouttime'));
	if(position=="first")
	{
		pointerClick(id+"-0",reqColorsArr[1],reqColorsArr[2],reqColorsArr[3],reqColorsArr[4],reqColorsArr[5],reqColorsArr[6],true,scrollCount,cid,numberofIndiv,delayTimeoutTime,id)
	}
	else
	{
		pointerClick(id+"-"+(totalNumberOfSlides-1),reqColorsArr[1],reqColorsArr[2],reqColorsArr[3],reqColorsArr[4],reqColorsArr[5],reqColorsArr[6],true,scrollCount,cid,numberofIndiv,delayTimeoutTime,id)
	}

}

var loopAnimation = function(totalElements,justId,numberOfAnim,currentNumber,ap_true)
{
		if($(totalElements[currentNumber]).hasClass('slideLeft')||$(totalElements[currentNumber]).hasClass('slideRight'))
				{
					$(totalElements[currentNumber]).animate({
						left:"0",
						opacity:"1"
					},{
						duration:parseFloat($(totalElements[currentNumber]).attr('data-action')),
						complete:function()
						{
							console.log(ap_true);
							currentNumber = currentNumber + 1;
							//alert(currentNumber+"--"+totalElements.length)
							console.log(currentNumber);
							if(currentNumber<totalElements.length)
							{
								loopAnimation(totalElements,justId,numberOfAnim,(currentNumber),ap_true);
								
							}
						}
					})
				}
		else if($(totalElements[currentNumber]).hasClass('slideTop')||$(totalElements[currentNumber]).hasClass('slideBottom'))
				{
					$(totalElements[currentNumber]).animate({
						top:"0",
						opacity:"1"
					},{
						duration:parseFloat($(totalElements[currentNumber]).attr('data-action')),
						complete:function()
						{
							console.log(ap_true);
							currentNumber = currentNumber + 1;
							//alert(currentNumber+"--"+totalElements.length)
							console.log(currentNumber);
							if(currentNumber<totalElements.length)
							{
								loopAnimation(totalElements,justId,numberOfAnim,(currentNumber),ap_true);
								
							}
						}
					})
				}

		else if($(totalElements[currentNumber]).hasClass('excelidefade'))
		{
			$(totalElements[currentNumber]).animate({
						opacity:"1"
					},{
						duration:parseFloat($(totalElements[currentNumber]).attr('data-action')),
						complete:function()
						{
							console.log(ap_true);
							currentNumber = currentNumber + 1;
							//alert(currentNumber+"--"+totalElements.length)
							console.log(currentNumber);
							if(currentNumber<totalElements.length)
							{
								loopAnimation(totalElements,justId,numberOfAnim,(currentNumber),ap_true);
								
							}
						}
					})
		}
}

var pointerClick = function(number,defaultWidth,animTimeoutTime,borderColor,color,currentBorderColor,currentColor,fromPointer,scrollCount_forSub,cid_forSub,options_forSub1,options_forSub2,justId_forSub)
		{

			var locked = false;
			if(!locked)
			{
				console.log(number);
			var combinedNumber = number;
			var tempArr = number.split('-');
			var justId = tempArr[0];
			cid = '#'+tempArr[0];

			var number = parseFloat(tempArr[1]);
			var defaultWidth = parseFloat(defaultWidth);
			var animTimeoutTime = parseFloat(animTimeoutTime);
			var animateType = $(cid).attr('data-animatetype');
			borderColor = '#'+borderColor;
			color = '#'+color;
			currentBorderColor = '#'+currentBorderColor;
			currentColor = '#'+currentColor
			console.log(currentBorderColor);
			console.log(currentColor);
			console.log(cid)
			console.log(number)
			
			console.log(defaultWidth);
			console.log(defaultWidth*number);


			$(''+cid+' .list_pointers .list_pointers_ul li').removeClass('activeli');
			$(''+cid+' .list_pointers .list_pointers_ul li a').removeClass('activepoint').attr('style',' border:2px solid '+borderColor+';background-color:'+color+';');
			//var currentLi = $(''+cid+' .list_pointers .list_pointers_ul li')[number];
			console.log('#'+combinedNumber)
			$(cid+' .ex_ul li.ex_li_indiv').removeClass('activeliMain');
			var MainList = $(cid+' .ex_ul li.ex_li_indiv');
			$(MainList[number]).addClass('activeliMain');
			$(cid+' #'+combinedNumber).attr('style','border:2px solid '+currentBorderColor+';background-color:'+currentColor+';').addClass('activepoint');
			var titleMain = $(cid+' .activeliMain').attr('data-title');
			$(cid+" .caption_container").html('');
			if(titleMain!=undefined)
			{
				$(cid+" .caption_container").html(titleMain).attr('title',titleMain)
			}
			else{
				$(cid+" .caption_container").html("&nbsp").attr('title',"")
			}
			$(cid+" .portrait_container").attr('style','background-image:url('+$(cid+' .activeliMain').attr('data-background')+')')
			

			if(fromPointer)
			{
				if($(cid).attr("pauseplay")=="false")
				{
					$(cid).attr("pauseplay",true);
					$(cid+" .playToggle i").attr('class','fa fa-play');
					//alert(cid);
					var tempvar = $(cid +" .activeliMain").find('.exanim');
					var numberOfAnim = tempvar.length;
					$('.testclick').html(numberOfAnim)
					var manipulatedTimeout = 0;
					for(var i=0;i<numberOfAnim;i++)
					{
						manipulatedTimeout = manipulatedTimeout + parseFloat($(tempvar[i]).attr('data-action'));
					}
					manipulatedTimeout = manipulatedTimeout + parseFloat($(cid).attr('data-delaytimeouttime'));
					console.log(manipulatedTimeout);
					$(cid).attr("pauseplay",false);
					$(cid+" .playToggle i").attr('class','fa fa-pause');
				}
			}



			var tempvar = $(cid +" .activeliMain").find('.exanim');
			var numberOfAnim = tempvar.length;
			console.log(tempvar.length)
				console.log($(tempvar[0]).attr('data-action')+"\n");
				if(numberOfAnim>0)
				{	

					$(cid+" .activeliMain .exanim").attr('style','opacity:0');
					loopAnimation(tempvar,justId,numberOfAnim,0,$(cid).attr('data-autoPlay'));
				}




			locked = true;
			switch(animateType)
			{
				case 'slide':
					console.log("slide");
					$(cid).animate(
					{
						easing:"swing",
						scrollLeft:parseFloat(defaultWidth)*parseFloat(number)
						
					},
					{
					duration: animTimeoutTime,
					complete:function()
						{
							console.log(parseFloat(defaultWidth))
							console.log("Done");
							locked = false;
							if(!fromPointer)
							{
								loopedSub(scrollCount_forSub,cid_forSub,options_forSub1,options_forSub2,justId_forSub)
							}
							//alert(1);
						}});
				break;
/*				case 'fade':
					console.log("fade");
					$(cid+' .ex_li_indiv').fadeOut(animTimeoutTime/2);
					$(cid+' .ex_li_indiv.activeliMain').fadeIn(animTimeoutTime,function(){
						loopedSub(scrollCount_forSub,cid_forSub,options_forSub1,options_forSub2,justId_forSub);
					});
				break;
				case 'drop':
					console.log("drop");
					$(cid+' .ex_li_indiv').hide(animTimeoutTime/2);
					$(cid+' .ex_li_indiv.activeliMain').show(animTimeoutTime/2);
				break;*/
/*				case 'push':
					console.log("push");
					$(cid+' .ex_li_indiv').animate({
						width:0,
						height:$(cid+' .ex_ul').height()
					},
					{
						duration:animTimeoutTime/2,
						complete:function()
						{

						}
					})
					$(cid+' .ex_li_indiv.activeliMain').animate({
						width:defaultWidth,
						height:$(cid+' .ex_ul').height()
					},
					{
						duration:animTimeoutTime/2
					})
					
				break;
				case 'fall':
					console.log("fall");
					$(cid+' .ex_li_indiv').animate({
						height:0
					},
					{
						duration:animTimeoutTime/2,
						complete:function()
						{
						}
					})
					$(cid+' .ex_li_indiv.activeliMain').animate({
						height:$(cid+' .ex_ul').height()
					},
					{
						duration:animTimeoutTime/2
					})
					
				break;*/
			}

			}
			
		}



var changeOneSlide = function(prev_next,id)
{
	var currentID = $('#'+id+' a.activepoint').attr('id');
	var colorString = $('#'+currentID).attr('data-colors');
	console.log('#'+id);
	var reqColorsArr = colorString.split(',');
	console.log(reqColorsArr);
	var totalNumberOfSlides = 0;
	var currentIDTemp = currentID.split('-');
	var currentIDno = currentIDTemp[currentIDTemp.length-1];
	var currentIDno = parseInt(currentIDno);

	var cid = '#'+id;
	var scrollCount = parseInt(currentIDno) + 1;
	totalNumberOfSlides = 0;
		$('#'+id+' .list_pointers_ul li').each(
			function()
			{
				totalNumberOfSlides = totalNumberOfSlides + 1;
			})
		var numberofIndiv = totalNumberOfSlides;
		var delayTimeoutTime = parseInt($(cid).attr('data-delaytimeouttime'));
	if(prev_next=="next")
	{

		currentIDno = currentIDno + 1;
		if(currentIDno<totalNumberOfSlides)
		{
			currentIDTemp = currentIDTemp[0].split('#');
			var currentViewIdno = currentIDTemp[currentIDTemp.length-1];
			pointerClick(currentViewIdno+'-'+currentIDno,reqColorsArr[1],reqColorsArr[2],reqColorsArr[3],reqColorsArr[4],reqColorsArr[5],reqColorsArr[6],true,scrollCount,cid,numberofIndiv,delayTimeoutTime,id)
		}
		else
		{
			//alert(currentIDno);
			currentIDno = 0;
			currentIDTemp = currentIDTemp[0].split('#');
			var currentViewIdno = currentIDTemp[currentIDTemp.length-1];
			pointerClick(currentViewIdno+'-'+currentIDno,reqColorsArr[1],reqColorsArr[2],reqColorsArr[3],reqColorsArr[4],reqColorsArr[5],reqColorsArr[6],true,scrollCount,cid,numberofIndiv,delayTimeoutTime,id)
		}
		
		
	}
	if(prev_next=="prev")
	{
		currentIDno = currentIDno - 1;
		if(currentIDno>=0)
		{
			currentIDTemp = currentIDTemp[0].split('#');
			var currentViewIdno = currentIDTemp[currentIDTemp.length-1];
			pointerClick(currentViewIdno+'-'+currentIDno,reqColorsArr[1],reqColorsArr[2],reqColorsArr[3],reqColorsArr[4],reqColorsArr[5],reqColorsArr[6],true,scrollCount,cid,numberofIndiv,delayTimeoutTime,id);
		}
		else
		{
			currentIDno = totalNumberOfSlides-1;
			currentIDTemp = currentIDTemp[0].split('#');
			var currentViewIdno = currentIDTemp[currentIDTemp.length-1];
			pointerClick(currentViewIdno+'-'+currentIDno,reqColorsArr[1],reqColorsArr[2],reqColorsArr[3],reqColorsArr[4],reqColorsArr[5],reqColorsArr[6],true,scrollCount,cid,numberofIndiv,delayTimeoutTime,id);
		}
		
		
	}
}



parentClass = function(elem,options,cid,justId,notedBody,pausePlay)
{
	this.methods = {
		initAgain:function()
		{
			$(cid).children().remove();
			$(cid).append(notedBody);
			console.log(notedBody);
			this.initAllEx(false)
			
			
		},
		initAllEx:function(notagain)
		{
			$(cid).attr('pausePlay',pausePlay);
			console.log(options);
			this.initViewVar(notagain);
			this.initBackImages();
			if(options.touchEnabled)
			{
				init();
				clickAndDrag(justId);
			}
		},
		initViewVar:function(notagain)
		{
			console.log(options);
			$(cid).append('<div class="list_pointers"><ul class="list_pointers_ul"></ul></div>');
			if(notagain)
			{
				notedBody = $(cid).children();
				notedBody = notedBody.prop('outerHTML');
				//console.log(notedBody)
			}
			if(options.animateType=="pile"
			||options.animateType=="jump"
			||options.animateType=="throw"
			||options.animateType=="fly"
			||options.animateType=="present"
			||options.animateType=="click"
			||options.animateType=="fadein"
			||options.animateType=="goo"
			||options.animateType=="zoom"
			)
			{
				$(cid+" .ex_li_indiv").addClass(options.animateType);
				options.animateType = "slide";
			}
			else if(options.animateType!="slide")
			{
				$(cid+" .ex_li_indiv").addClass('slide');
				options.animateType = "slide";
			}
			$(cid).attr('data-animateType',"");
			$(cid).attr('data-animateType',options.animateType);
			$(cid).attr('data-autoPlay',"");
			$(cid).attr('data-autoPlay',options.autoPlay);
			$(cid).attr('data-delayTimeoutTime',"");
			$(cid).attr('data-delayTimeoutTime',options.delayTimeoutTime);
			$(cid).attr('pauseOnHover',"");
			$(cid).attr('pauseOnHover',options.pauseOnHover);
			if(options.portraitMode)
			{
				$(cid).attr('portraitMode',options.portraitMode).addClass('portraitMode');
				$(cid+" .ex_li_indiv").addClass('slide');
				options.animateType = "slide";
				//$(cid);
			}
			else
			{
				$(cid).attr('portraitMode',false);
			}
			
			if(options.portraitMode)
			{
				$(cid+" .ex_ul").append('<div class="portrait_container"><div class="caption_container"></div></div>');
				if(options.portraitTheme=="dark")
				{
					$(cid+" .ex_ul .portrait_container").addClass('dark');
				}
			}
			if(options.pauseOnHover)
			{
				mouseEnterLeaveHandler();
			}

		},
		initBackImages:function()
		{
			console.log(notedBody);
			console.log(options);
			var indivCount = 0;
			var backupNo = 0;
			options.numberofIndiv = 0;
			var transitionDuration = options.animTimeoutTime;
			transitionDuration = parseFloat(transitionDuration)/1000;
			transitionDuration = transitionDuration + "s";
			console.log(cid);
			$(cid).remove('#excelider_imgs_'+justId);
			$(cid).append('<div class="images_backup" id="excelider_imgs_'+justId+'"></div>');
			$(cid).append('<div class="images_tooltipCont" id="excelider_tooltip_cont"></div>');
			$(cid+' .ex_li_indiv').each(
				function()
				{
					indivCount = indivCount + 1;
					var imageBlock = $(this).children('img.ex_backimg').remove()
					$(cid+" #excelider_imgs_"+justId).append(imageBlock)
					$(cid+" #excelider_tooltip_cont").append('<div class="content_images" id="tt_'+justId+'_'+(indivCount-1)+'">'+imageBlock.prop('outerHTML')+'</div>')
					
				})
			$(cid+' #excelider_imgs_'+justId+' img').each(
				function()
				{
					backupNo = backupNo + 1;
					console.log('Children'+backupNo);
					console.log($(this).children($('img')));
					$(cid+' .ex_li_indiv:nth-child('+backupNo+')').attr("style","background-image:url("+$(this).attr('src')+");transition:"+transitionDuration+";").attr("data-background",$(this).attr('src'));
					//$(this).attr("style","background-image:url("+$(this).children($('img')).attr('src')+")");
					console.log($(this).children($('img')).attr('src'));
					options.numberofIndiv = options.numberofIndiv + 1;
					console.log(options.numberofIndiv);
				});
			this.loadTheSizes();
		},
		responsiveWidth:function(fullWidthPercent)
		{
			if(fullWidthPercent!=undefined)
			{
				var percent = ($(window).width()*fullWidthPercent)/100;
				console.log(percent)
			}
			
			if(options.responsive)
			{
				options.defaultWidth = parseInt(percent);
				options.defaultHeight = options.defaultHeight;
			}
			else{
				options.defaultWidth = options.defaultWidth;;
				options.defaultHeight = options.defaultHeight;
			}
		},
		loadTheSizes:function()
		{	

			console.log(typeof(options.fullWidthPercent));
			if(typeof(options.fullWidthPercent)=="number")
			{
				this.responsiveWidth(options.fullWidthPercent);
			}

			console.log(options);
			//alert(options.numberofIndiv);
					$(cid+' .ex_ul').width(options.defaultWidth*options.numberofIndiv);
					$(cid+' .ex_ul').height(options.defaultHeight);
					$(cid+' .ex_li_indiv').width(options.defaultWidth);
					$(cid+' .ex_li_indiv').height(options.defaultHeight);			
					$(cid+'.ex_viewport').width(options.defaultWidth);
					$(cid+'.ex_viewport').height(options.defaultHeight);
					$(cid+'.ex_viewport').parent('.excelider_container').width(options.defaultWidth);
					$(cid+'.ex_viewport').parent('.excelider_container').height(options.defaultHeight);
					
						this.initSliderPoint();
						this.startRolling();
					
		},
		initSliderPoint:function()
		{
			$(cid+" .playControls").remove();
			if(options.showPlayControls)
			{
				$(cid).append('<div class="playControls" id="'+justId+'_playControl"><div class="play_block"><button title="First" class="play_blockBtn" onclick=changeExtreme("first","'+justId+'")><i class="fa fa-fast-backward" aria-hidden="true"></i></button></div><div class="play_block"> <button class="play_blockBtn" title="Previous" onclick=changeOneSlide("prev","'+justId+'")><i class="fa fa-chevron-left" aria-hidden="true"></i></button></div><div class="play_block"><button title="Play/Pause" class="play_blockBtn playToggle" onclick=playToggle("'+justId+'")><i class="fa fa-pause" aria-hidden="true"></i></button></div><div class="play_block"> <button title="Next" class="play_blockBtn" onclick=changeOneSlide("next","'+justId+'")><i class="fa fa-chevron-right" aria-hidden="true"></i></button></div><div class="play_block"> <button class="play_blockBtn" title="Last" onclick=changeExtreme("last","'+justId+'")><i class="fa fa-fast-forward" aria-hidden="true"></i></button></div></div>');
				if(!options.autoPlay)
				{
					setTimeout(
						function()
						{
							$(cid+" .playToggle").addClass('disabled');
						},500)
				}
				//Position of Controls
				if(options.playControls.position==""||options.playControls.position==undefined)
				{
					$(cid+' .playControls').addClass("topleft");
				}
				else
				{
					$(cid+' .playControls').addClass(options.playControls.position);
				}
			}
			if(options.showNav)
			{
				$(cid).append('<button class="nav_buttons excelider_prev" id="'+justId+'-prev" onclick=changeOneSlide("prev","'+justId+'")><i class="fa fa-chevron-left" aria-hidden="true"></i></button>');
				$(cid).append('<button class="nav_buttons excelider_next" id="'+justId+'-next" onclick=changeOneSlide("next","'+justId+'")><i class="fa fa-chevron-right" aria-hidden="true"></i></button>');

				if(options.navOptions.autoHide)
				{
					$(cid).addClass('showNavHover');
					$(cid+" .nav_buttons").addClass('autoHide');
				}
				
				if(options.navOptions.navHeight == "lg")
				{
					$(cid+" .nav_buttons").addClass('lg')
				}
				else if(options.navOptions.navHeight == "md")
				{
					$(cid+" .nav_buttons").addClass('md')
				}
				else
				{
					$(cid+" .nav_buttons").addClass('sm')
				}
				

			}
			
			

			$(cid+' .list_pointers_ul').html('');
			var OnlyId = cid.split('#');
			console.log(OnlyId)
			OnlyId = OnlyId[OnlyId.length-1];

			var onlyColors = {};
			onlyColors.bc = rgb2hex(options.slidePoint.borderColor)
			onlyColors.c = rgb2hex(options.slidePoint.color);
			onlyColors.cbc = rgb2hex(options.slidePoint.currentBorderColor);
			onlyColors.cc = rgb2hex(options.slidePoint.currentColor);
			console.log(onlyColors);
			//$(cid).children($('.list_pointers_ul').children().remove());
			$(cid+' .list_pointers_ul').html('');

			for(var i=0;i<options.numberofIndiv;i++)
			{
					$(cid+' .list_pointers_ul').append('<li><a data-tooltip-content="#tt_'+justId+'_'+i+'" id="'+OnlyId+'-'+i+'" style="border:2px solid '+options.slidePoint.borderColor+';background-color:'+options.slidePoint.color+';" onclick=pointerClick("'+OnlyId+'-'+i+'","'+options.defaultWidth+'","'+options.animTimeoutTime+'","'+onlyColors.bc+'","'+onlyColors.c+'","'+onlyColors.cbc+'","'+onlyColors.cc+'",true) data-colors='+OnlyId+'-'+i+','+options.defaultWidth+','+options.animTimeoutTime+','+onlyColors.bc+','+onlyColors.c+','+onlyColors.cbc+','+onlyColors.cc+' class="pointer_linkno tooltipImage" href="javascript:void(0)"></a></li>');
			}

			$('.tooltipImage').tooltipster({
					   animation: 'grow',
					   delay: 200,
					   theme: 'tooltipster-punk',
					   trigger: 'hover'
					});
			//Border for POinters
			if(options.slidePoint.noBorder)
			{
				$(cid+" .pointer_linkno").addClass('noborder');
			}
			if(options.showSlidePoint==false)
			{
				$(cid+" .pointer_linkno").addClass('invisible');
			}
			if(options.slidePoint.pointType=="square")
			{
				$(cid+" .pointer_linkno").addClass('squared');
			}
			pointerClick(OnlyId+'-0',options.defaultWidth,options.animTimeoutTime,onlyColors.bc,onlyColors.c,onlyColors.cbc,onlyColors.cc,false,1,cid,options.numberofIndiv,options.delayTimeoutTime,justId);	
		},
		startRolling:function()
		{
			if(options.autoPlay==true)
			{
				this.loopedMain();
			}
			else
			{

			}
		},
		pointerClick:function(number)
		{
			$(cid).animate(
			{
				scrollLeft:parseFloat(options.defaultWidth)*parseFloat(number)
				
			},options.animTimeoutTime);
			$(cid).find($('.list_pointers_ul li').removeClass('activeli'));
			$(cid).find($('.list_pointers_ul li a').attr('style',' border:2px solid '+options.slidePoint.borderColor+';background-color:'+options.slidePoint.color+';').removeClass('activepoint'));
			$(cid).find($('.list_pointers_ul li')[number]).addClass('activeli');
			$(cid).find($('.list_pointers_ul li')[number]).children('a.pointer_linkno').addClass('activepoint').attr('style','border:2px solid '+options.slidePoint.currentBorderColor+';background-color:'+options.slidePoint.currentColor+';');
		},
		loopedMain:function()
		{
			//loopedSub(1,cid,options,justId);
		}


	}
}

